CLUSTER_NAME := podtato-cluster
CONTEXT := kind-$(CLUSTER_NAME)

.PHONY: create-cluster
create-cluster:
	kind create cluster --name $(CLUSTER_NAME) --config kind.yaml

.PHONY: install-flux
install-flux:
	kubectl --context $(CONTEXT) apply -f https://github.com/fluxcd/flux2/releases/latest/download/install.yaml

.PHONY: init-flux
init-flux:
	kubectl --context $(CONTEXT) apply -f ./flux-init

.PHONY: setup
setup:
	$(MAKE) create-cluster install-flux init-flux

.PHONY: delete-cluster
delete-cluster:
	kind delete cluster --name $(CLUSTER_NAME)

.PHONY: clean
clean:
	$(MAKE) delete-cluster
